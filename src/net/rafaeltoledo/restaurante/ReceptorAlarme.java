package net.rafaeltoledo.restaurante;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class ReceptorAlarme extends BroadcastReceiver {
	
	private static final int ID_NOTIFICACAO = 67234;

	@Override
	public void onReceive(Context context, Intent intent) {
		SharedPreferences preferencias = PreferenceManager.getDefaultSharedPreferences(context);
		boolean usarNotificacao = preferencias.getBoolean("usar_notificacao", true);
		
		if (usarNotificacao) {			
			NotificationManager gerenciador = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
			Notification nota = new Notification(R.drawable.notificacao, context.getString(R.string.hora_do_almoco), System.currentTimeMillis());
			PendingIntent i = PendingIntent.getActivity(context, 0, new Intent(context, AlarmeActivity.class), 0);
			nota.setLatestEventInfo(context, context.getString(R.string.app_name), context.getString(R.string.notificacao), i);
			nota.flags |= Notification.FLAG_AUTO_CANCEL;
			gerenciador.notify(ID_NOTIFICACAO, nota);
		} else {		
			Intent i = new Intent(context, AlarmeActivity.class);
			i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);		
			context.startActivity(i);
		}
	}
}
