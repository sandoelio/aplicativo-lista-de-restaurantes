package net.rafaeltoledo.restaurante;

import android.app.IntentService;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Intent;
import android.database.Cursor;
import android.widget.RemoteViews;

public class WidgetService extends IntentService {

	public WidgetService() {
		super("WidgetService");		
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		ComponentName cn = new ComponentName(this, WidgetAplicativo.class);
		RemoteViews atualizarFrame = new RemoteViews("net.rafaeltoledo.restaurante", R.layout.widget);
		GerenciadorRestaurantes gerenciador = new GerenciadorRestaurantes(this);
		AppWidgetManager mgr = AppWidgetManager.getInstance(this);
		
		try {
			Cursor c = gerenciador.getReadableDatabase().rawQuery("SELECT COUNT(*) FROM restaurantes", null);
			c.moveToFirst();
			int contador = c.getInt(0);
			c.close();
			
			if (contador > 0) {
				int deslocamento = (int) (contador * Math.random());
				String args[] = {String.valueOf(deslocamento)};
				c = gerenciador.getReadableDatabase().rawQuery("SELECT _id, nome FROM restaurantes LIMIT 1 OFFSET ?", args);
				c.moveToFirst();
				atualizarFrame.setTextViewText(R.id.nome, c.getString(1));				
				
				Intent i = new Intent(this, FormularioDetalhes.class);
				i.putExtra(ListaRestaurantes._ID, c.getString(0));
				PendingIntent pi = PendingIntent.getActivity(this, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);
				atualizarFrame.setOnClickPendingIntent(R.id.nome, pi);
				
				c.close();
			} else {
				atualizarFrame.setTextViewText(R.id.nome, getString(R.string.vazio));
			}
		} finally {
			gerenciador.close();
		}
		
		Intent i = new Intent(this, WidgetService.class);
		PendingIntent pi = PendingIntent.getService(this, 0, i, 0);
		atualizarFrame.setOnClickPendingIntent(R.id.proximo, pi);
		mgr.updateAppWidget(cn, atualizarFrame);
	}
}
